FROM ubuntu:18.04

LABEL maintainer="tomer.klein@gmail.com"
RUN apt update -yqq && \
    apt install -yqq python-pip && \
    apt install -yqq curl && \
    apt install -yqq speedtest-cli && \
    apt install -yqq wget
    


RUN pip install docker --no-cache-dir && \
    pip install telepot --no-cache-dir

RUN mkdir /opt/idx

COPY requirements.txt /opt/idx

RUN pip install -r /opt/idx/requirements.txt

COPY idx.py /opt/idx

ENTRYPOINT ["/usr/bin/python", "/opt/idx/idx.py"]