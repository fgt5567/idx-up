import requests
import telegram
import json

from telegram.forcereply import ForceReply
from telegram.ext import Updater
from telegram.ext import CommandHandler

telegram_bot_token = "2133086187:AAERqpZzLjv8uYzABrsFt_7stWYTyg_mJmo"

updater = Updater(token=telegram_bot_token, use_context=True)
dispatcher = updater.dispatcher

def harga(update, context):
    chat_id = update.effective_chat.id

    pair = context.args[0]

    base_url = "https://indodax.com/api/ticker"

    coin = f"{base_url}/{pair}idr"

    response = requests.get(coin)

    if response.json().get("ticker"):
        text = response.json().get("ticker").get("last")
        text = "{:0,}".format(float(text))
        text = f"Harga {pair} Rp {text} saat ini!"
    else:
        text = f"{pair} ini coin apa ?!"
    context.bot.send_message(chat_id=chat_id, text=text)

def help(update, context):
    chat_id = update.effective_chat.id
    content = "coin+harga_pair \n contoh : /harga doge"
    context.bot.send_message(chat_id=chat_id, text=content)

dispatcher.add_handler(CommandHandler("harga", harga))
dispatcher.add_handler(CommandHandler("help", help))

updater.start_polling()
